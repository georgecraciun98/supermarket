import java.util.*;

public class myTimer_1 {
    int limit_nr;
    my_view view;
    myTimer_1(int limit,my_view a){
        view=a;
        limit_nr=limit;
    }
       private static int secondsPassed=0;
        // creating timer task, timer
    Timer myTimer=new Timer();
        TimerTask tasknew = new TimerTask() {
            @Override
            public void run() {
                secondsPassed++;
                if (secondsPassed >= limit_nr+1) {
                    myTimer.cancel();
                    myTimer.purge();
                    return;
                }
                view.text7.setText("Time passed "+converting(secondsPassed));

            }

        };
        public void start(){
            myTimer.scheduleAtFixedRate(tasknew,0,1000);

        }
        public int get_seconds()
        {
            return secondsPassed;
        }
        static String converting(int sec){
        int hours=0;
        int remainderHours=0;
        int minutes=0;
        int seconds=0;
        if(sec>=3600){
            hours=sec/3600;
            remainderHours=sec%3600;
            if(remainderHours>=60){
                minutes=remainderHours/60;
                seconds=remainderHours%60;
            }
            else
            {
                seconds=remainderHours;
            }


        }
        else if(sec>=60){
         hours=0;
         minutes=sec/60;
         seconds=sec%60;
        }
        else if(sec<60){
           hours=0;
           minutes=0;
           seconds=sec;
        }
        String shours;
        String sminutes;
        String sseconds;
        if(seconds<10) {
            sseconds="0"+Integer.toString(seconds);
        }
        else
            sseconds=Integer.toString(seconds);
        if(minutes<10) {
                sminutes="0"+Integer.toString(minutes);
            }
        else
                sminutes=Integer.toString(seconds);
        if(hours<10) {
                shours="0"+Integer.toString(hours);
            }
        else
                shours=Integer.toString(hours);
        return ""+shours+":"+sminutes+":"+sseconds;
        }

}