import java.util.*;
import java.lang.Thread;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

public class checkOut extends Thread{
   int class_id;
   int min_service_time=2000;
   int max_service_time=6000;
   int diference_time=max_service_time-min_service_time;
   int finish_time=0;
   int previous_1=0;
    int previous_2=0;
    int previous_3=0;
   public static int average_time;

   List<Client> my_queue;
     static myTimer_1 t;
    my_view view;

   checkOut(int a,my_view a1) {
       view=a1;
       view.a1.setText("");
       class_id = a;
       my_queue = new LinkedList<>();
   }
   public void set_finish(){
       finish_time=1;
   }
   public synchronized void removeClient(int service) throws InterruptedException{
      while( my_queue.size() == 0 )
         wait();
      Client c =my_queue.get(0);
     my_queue.remove(0);
     c.leaving_time=t.get_seconds();
     c.service_time=service;
     c.waiting_time=c.leaving_time-c.arriving_time;
     average_time+=c.waiting_time;
      view.a1.append(c.get_info()+" and he was served by checkout number  "+(class_id+1)+"\n");
      notifyAll();
   }
   public void run() {
      try{
    while(finish_time==0){
        Random rand = new Random();
        int randomNum = rand.nextInt(diference_time);

        int x=min_service_time+randomNum;
        sleep(x*1);
        removeClient(x);

    }
      }catch(InterruptedException e){

         System.out.println("Interruption");
         System.out.println(e.getMessage());
      }
}

  public synchronized void addClient(Client a,myTimer_1 t1) throws InterruptedException{
     my_queue.add(a) ;
     t=t1;
     notifyAll();
  }
  public synchronized int get_length() throws InterruptedException{

       notifyAll();
       int size=my_queue.size();

       if(class_id==0){
           if(size!=previous_1){
           view.text1.setText("");
           if(size!=0){
               for(Client x:my_queue) {
                   view.text1.setText(view.text1.getText() + " "+(x.index+1));
               }
               previous_1=size;
           }}
       }
      if(class_id==1){
          if(size!=previous_2){
          view.text3.setText("");
          if(size!=0){
              for(Client x:my_queue) {
                  view.text3.setText(view.text3.getText() + " "+(x.index+1));
              }
              previous_2=size;
          }}
      }
      if(class_id==2){
          if(size!=previous_3){
          view.text5.setText("");
          if(size!=0){
              for(Client x:my_queue) {
                  view.text5.setText(view.text5.getText() + " "+(x.index+1));
              }
              previous_3=size;
          }
      }}
       return size;
  }

}


