import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class My_Controller {
	private my_view view;

	My_Controller( my_view v) {
		view = v;
		view.addb1Listener(new b1Listener());
		view.addb2Listener(new b2Listener());
		view.addb3Listener(new b3Listener());
	}


	class b1Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			try {

            view.simulation_start=1;
			} catch (NumberFormatException nfex) {

			}
		}
	}
	class b2Listener implements ChangeListener{


		@Override
		public void stateChanged(ChangeEvent e) {
			try{
				view.number_customers=(Integer)view.b2.getValue();
			}catch(Exception e1){
				System.out.println(e1.getMessage());
			}
		}
	}
	class b3Listener implements ChangeListener{


		@Override
		public void stateChanged(ChangeEvent e) {
			try{
				view.number_queues=(Integer)view.b3.getValue();
			}catch(Exception e1){
				System.out.println(e1.getMessage());
			}
		}
	}

}




