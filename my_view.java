import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.ChangeListener;

public class my_view extends JFrame{
	
	
	JTextField text1=new JTextField(10);
	JTextField text2=new JTextField("Checkout 1");
	JTextField text3=new JTextField(10);
	JTextField text4=new JTextField("Checkout 2");
	JTextField text5=new JTextField(10);
	JTextField text6=new JTextField("Checkout 3");
	JTextField text7=new JTextField("Simulation time");
	JTextField text8=new JTextField();
	JTextArea a1=new JTextArea(10,10);
	JButton b1=new JButton("Start Simulation");
	JSpinner b2=new JSpinner();
	JSpinner b3=new JSpinner();
    int simulation_start=0;
    int number_customers=0;
    int number_queues=0;

	my_view(){
    	JFrame frame=new JFrame("Checkout in Supermarket");
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setSize(350, 350);
    	GridLayout grid=new GridLayout(4,3,8,6);

    	JPanel p1=new JPanel();
    	JScrollPane scroll=new JScrollPane(a1);
    	scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    	p1.setLayout(grid);
		Font font1 = new Font("SansSerif", Font.BOLD, 20);
        text1.setFont(font1);
		text4.setFont(font1);
		text6.setFont(font1);
		text3.setFont(font1);
		text5.setFont(font1);
		text2.setFont(font1);
		a1.setFont(font1);
		text7.setFont(font1);
		text8.setFont(font1);
		b1.setFont(font1);
        b2.setFont(font1);
        b3.setFont(font1);
    	p1.add(text2);
    	p1.add(text1);
        p1.add(b1);
		p1.add(text4);
		p1.add(text3);
        p1.add(text7);

		p1.add(text6);
		p1.add(text5);
        p1.add(text8);

    	p1.add(scroll);

    	p1.add(b2);
    	p1.add(b3);
    	frame.setContentPane(p1);
		frame.setVisible(true);

    }
    void addb1Listener(ActionListener x)
    {
    	b1.addActionListener(x);
    }
    void addb2Listener(ChangeListener x)
    {
    	b2.addChangeListener(x);
    }

	void addb3Listener(ChangeListener x)
	{
		b3.addChangeListener(x);
	}

    
	
}
