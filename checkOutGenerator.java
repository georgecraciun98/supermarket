import static java.lang.Thread.sleep;

import java.util.Random;
import java.util.Timer;;
import java.util.TimerTask;

public class checkOutGenerator extends Thread {
private checkOut checkOuts[];
private int nr_queues;
private int nr_customers;
public int min_arriving=500;
public int max_arriving=1000;
public int dif_arriving=max_arriving-min_arriving;
public int limit;
myTimer_1 time;
    my_view view;
public checkOutGenerator(my_view v,int nr_q,int nr_c,checkOut a1[],int l){
view=v;
nr_queues=nr_q;
nr_customers=nr_c;
checkOuts=new checkOut[nr_q];
limit=l;
int i=0;
while(i<nr_q){
    checkOuts[ i ] = a1[i];
    i++;
}
    time=new myTimer_1(limit,v);
    time.start();
}

public int get_index(){
 int my_index=0;

 try{
  int i1=checkOuts[0].get_length();
  int i=1;
  while(i<nr_queues){

      if(checkOuts[i].get_length()<i1){
          i1=checkOuts[i].get_length();
          my_index=i;
      }
      i++;
  }

 } catch (InterruptedException e) {
     System.out.println(e.getMessage());
 }

return my_index;
}

public void run(){
int count=1;
  try{

  for(int i=0;i<nr_customers;i++){
      int arriving_time=time.get_seconds();
   Client a=new Client(i,arriving_time);
   int min=get_index();
   checkOuts[min].addClient(a,time);
      Random rand = new Random();
      int randomNum = rand.nextInt(dif_arriving);
      int x=randomNum+min_arriving;
      sleep( x*1 );

  int count2=0;
    if(i==nr_customers-1) {
        int count1=1;
       while(count1<=nr_queues)
       {     count2=0;
           for (int j = 0; j < nr_queues; j++)
            if(checkOuts[j].get_length()==0&&checkOuts[j].finish_time!=-1||time.get_seconds()>limit) {
                count2+=checkOuts[j].get_length();
                checkOuts[j].stop();
                checkOuts[j].finish_time=-1;
                count1++;
            }

       }


       if(time.get_seconds()>limit)
       {    view.a1.append("\n"+"Simulation has reached the time limit");
           view.a1.append("\n"+"Clients had an average time of waiting :"+checkOuts[0].average_time/(nr_customers-count2));
           view.a1.append("\nWe served only "+(nr_customers-count2)+" customers");}
       else
           view.a1.append("\n"+"Clients had an average time of waiting :"+checkOuts[0].average_time/nr_customers);
    }

  }

  }
  catch(InterruptedException e){
      System.out.println(e.getMessage());
  }

}
}
