import java.util.Timer;;
import java.util.TimerTask;

public class Main {

    public static void main(String[] args) {
        int nr_q = 0;
        int nr_c = 0;
        checkOut checkOuts[];
        int i = 0;
        int limit=0;
        int ok=0;
        int ok1=0;
        int ok2=0;
        my_view v=new my_view();
        My_Controller controler=new My_Controller(v);
        while(ok==0&&ok1==0&&ok2==0){
        try{
            limit=Integer.parseInt(v.text8.getText());
            nr_c=v.number_customers;
            nr_q=v.number_queues;
            if(limit>20&&nr_c>5&&nr_q>=1&&nr_q<4&&nr_c<120)
            {  ok=1;
                ok1=1;
                ok2=1;
            }
        }
        catch(Exception e){
            v.a1.setText("First set number_customers>5\nThen set number_queues<=3\nThen set simulation_time>20");
        }}

        while(v.simulation_start==0){
            v.a1.setText("Press start simulation ");
        }
        checkOuts = new checkOut[nr_q];
        if(ok==1&&ok1==1&&ok2==1) {
            for (i = 0; i < nr_q; i++) {
                checkOuts[i] = new checkOut(i, v);
                checkOuts[i].start();

            }

            checkOutGenerator c = new checkOutGenerator(v, nr_q, nr_c, checkOuts,limit);
            c.start();
        }
}}
